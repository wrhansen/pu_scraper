import argparse
import sys

from bs4 import BeautifulSoup
import requests

def main(argv):
	parser = argparse.ArgumentParser(prog=argv[0], description=__doc__)
	parser.add_argument("--sku", action="store", default="1800-1325", help="The sku to lookup.")
	args = parser.parse_args(argv[1:])
	params = {
		'partNumber': args.sku.replace('-', '').strip()
	}
	response = requests.get('http://www.parts-unlimited.com/products/', params=params)
	bs_response = BeautifulSoup(response.text, "html.parser")

	# Retrieve part metadata
	print("Details: ")
	part_metadata = bs_response.find(id="partCopy")
	metadata = []
	element_pair = []
	for element in part_metadata.find_all(['h3', 'p']):
		element_pair.append(element.text)
		if len(element_pair) == 2:
			metadata.append(element_pair)
			element_pair = []
	print("\n".join("{}: {}".format(*pair) for pair in metadata))
	print("*"*80)

	# Specifications
	parts_specifications = bs_response.find(id="partSpecsWrap")
	specification_headers = []
	specification_values = []
	print("Specifications: ")
	for element in parts_specifications.find_all("th", class_="nameCol"):
		specification_headers.append(element.text)
	for element in parts_specifications.find_all("td", class_="valCol"):
		specification_values.append(element.text)
	print("\n".join("{}: {}".format(*pair) for pair in zip(specification_headers, specification_values)))
	print("*"*80)

	# Fitment
	print("Fitment: ")
	fitment_table_data = bs_response.find("table", class_="partFitmentList")
	fitment_data = fitment_table_data.find("tbody")
	fitment = []
	for idx, element in enumerate(fitment_data.find_all("tr")):
		if idx == 0:
			continue  # Skip header row
		fitment_row = [thing.text for thing in element.find_all("td")[:-1]]
		fitment.append(fitment_row)

	print("\n".join("{:<20} {:<50} {:11}".format(*fitment_row) for fitment_row in fitment))
	print("*"*80)


if __name__ == '__main__':
	sys.exit(main(sys.argv))
